# README

## About
extr is a bash script to extract many different compressed file formats with a unified interface.

extr is designed to provide a basic interface for different modules to attach onto. Each module is responsible for a different type of file extraction.

extr is licensed under the GPLv3+ as are the included modules. However, modules that a user writes for themselves are under no restriction to use the GPL for those modules.

## Examples
```
#extract a file into the current directory
extr tarfile.tar.gz

#extract a file into a target directory
extr gzfile.gz -o output_dir/

#extract a file and overwrite any existing target files
extr gzfile.gz -o output_dir/ -f

#Suppress all output
extr zstdfile.zstd -q

#Enable verbose output (exact output is determined by the underlying utility)
extr xzfile.xz -v
```

## Installation

extr will look in both "$HOME/.bashfiles/extr-modules/" and "/etc/extr-modules/" for any modules to load. Install the modules files into either of those locations and extr will load them automatically. extr itself should be placed in directory in the user's PATH. /usr/bin, $HOME/bin, wherever your PATH and permissions allow.
